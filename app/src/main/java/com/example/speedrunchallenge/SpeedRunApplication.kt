package com.example.speedrunchallenge

import android.app.Application
import com.example.speedrunchallenge.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class SpeedRunApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SpeedRunApplication)
            modules(appModule)
        }
    }
}