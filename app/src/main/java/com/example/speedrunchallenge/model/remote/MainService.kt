package com.example.speedrunchallenge.model.remote

import com.example.speedrunchallenge.model.local.entities.CountriesResponse
import com.example.speedrunchallenge.model.local.entities.Country
import io.ktor.client.statement.HttpResponse

interface MainService {

    suspend fun getCountries(): List<Country>

    companion object{
        fun create(): MainService{
            return MainServiceImpl()
        }
    }
}