package com.example.speedrunchallenge.model

import android.util.Log
import com.example.speedrunchallenge.model.local.entities.Country
import com.example.speedrunchallenge.model.remote.MainService

class MainRepo(
    private val service: MainService
) {

    suspend fun getCountries(): List<Country> {
        val countries = service.getCountries()
        return try {
            countries
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Fetching countries from the network failed in the repo. ${e.localizedMessage}"
            )
            emptyList<Country>()
        }
    }

    companion object {
        const val TAG = "MainRepo"
    }
}