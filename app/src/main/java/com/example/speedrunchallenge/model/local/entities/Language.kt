package com.example.speedrunchallenge.model.local.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Language(
    val code: String? = null,
    @SerialName("iso639_2")
    val isoName: String? = null,
    val name: String,
    val nativeName: String? = null
)