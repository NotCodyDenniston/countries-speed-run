package com.example.speedrunchallenge.model.local.entities

import kotlinx.serialization.Serializable

@Serializable
class CountriesResponse : ArrayList<Country>()