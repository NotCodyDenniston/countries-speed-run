package com.example.speedrunchallenge.model.remote

import android.util.Log
import com.example.speedrunchallenge.model.local.entities.Country
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.android.Android
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class MainServiceImpl : MainService {
    override suspend fun getCountries(): List<Country> {
        val client = HttpClient(Android)
        val json = Json {
            ignoreUnknownKeys = true
            prettyPrint = true
            isLenient = true
        }
        return try {
            val response = client.get(BASE_URL).body<String>()
            val decodedResponse: List<Country> = json.decodeFromString(response)
            decodedResponse
        } catch (e: Exception) {
            Log.e(TAG, "Error fetching and decoding HTTPS Response. ${e.localizedMessage}")
            emptyList<Country>()
        }
    }

    companion object {
        const val TAG = "MainServiceImpl"
        const val BASE_URL =
            "https://gist.githubusercontent.com/peymano-wmt/32dcb892b06648910ddd40406e37fdab/raw/db25946fd77c5873b0303b858e861ce724e0dcd0/countries.json"
    }
}