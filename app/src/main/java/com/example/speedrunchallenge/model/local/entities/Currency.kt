package com.example.speedrunchallenge.model.local.entities

import kotlinx.serialization.Serializable

@Serializable
data class Currency(
    val code: String,
    val name: String,
    val symbol: String? = null
)