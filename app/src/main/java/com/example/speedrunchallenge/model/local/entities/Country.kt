package com.example.speedrunchallenge.model.local.entities

import kotlinx.serialization.Serializable

@Serializable
data class Country(
    val capital: String,
    val code: String,
    val currency: Currency,
    val demonym: String? = null,
    val flag: String,
    val language: Language,
    val name: String,
    val region: String
)