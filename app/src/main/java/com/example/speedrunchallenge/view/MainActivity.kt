package com.example.speedrunchallenge.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.AbsoluteRoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.speedrunchallenge.model.local.entities.Country
import com.example.speedrunchallenge.model.local.entities.Currency
import com.example.speedrunchallenge.model.local.entities.Language
import com.example.speedrunchallenge.ui.theme.SpeedRunChallengeTheme
import com.example.speedrunchallenge.viewmodel.MainViewModel
import org.koin.androidx.compose.getViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            SpeedRunChallengeTheme {
                val mainViewModel = getViewModel<MainViewModel>()
                mainViewModel.getCountries()
                val state = mainViewModel.state.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DisplayCountriesScreen(countries = state.value)
                }
            }
        }
    }
}

@Composable
fun DisplayCountriesScreen(
    countries: List<Country>
) {
    LazyColumn(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(top = 48.dp, start = 16.dp, end = 16.dp)
    ) {
        items(countries) { country ->
            Card(
                Modifier
                    .fillMaxSize()
                    .padding(24.dp)
                    .border(
                        width = 2.dp,
                        color = Color.LightGray,
                        shape = AbsoluteRoundedCornerShape(10.dp)
                    )
            ) {
                Row(horizontalArrangement = Arrangement.SpaceBetween) {
                    cardText(
                        text = country.name + ", " + country.region,
                        modifier = Modifier.weight(1f)
                    )
                    cardText(text = country.code)
                }
                cardText(text = country.capital)
            }
        }
    }
}

@Composable
fun cardText(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        fontSize = 24.sp,
        modifier = modifier
            .padding(16.dp)
    )
}

