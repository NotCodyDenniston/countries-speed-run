package com.example.speedrunchallenge.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.speedrunchallenge.model.MainRepo
import com.example.speedrunchallenge.model.local.entities.Country
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel(
    private val repo: MainRepo
) : ViewModel() {
    private val _state = MutableStateFlow<List<Country>>(emptyList())
    val state: StateFlow<List<Country>> get() = _state


     fun getCountries() = viewModelScope.launch {
      //  try {
            _state.value = repo.getCountries()
//        } catch (e: Exception) {
//            Log.e(TAG, "Error fetching countries in the viewmodel. ${e.localizedMessage}")
//        }
    }

    companion object {
        const val TAG = "MainViewModel"
    }
}