package com.example.speedrunchallenge.di

import com.example.speedrunchallenge.model.MainRepo
import com.example.speedrunchallenge.model.remote.MainService
import com.example.speedrunchallenge.viewmodel.MainViewModel
import org.koin.dsl.module

val appModule = module {
    single {
        MainService.create()
    }
    single {
        MainRepo(get())
    }
    single {
        MainViewModel(get())
    }
}