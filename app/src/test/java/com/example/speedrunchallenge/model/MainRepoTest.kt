package com.example.speedrunchallenge.model

import com.example.speedrunchallenge.model.remote.MainService
import com.example.speedrunchallenge.utils.CoroutinesTestExtension
import com.example.speedrunchallenge.viewmodel.FAKE_COUNTRIES
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class MainRepoTest {

    private val service: MainService = mockk(relaxed = true)
    private val repo = MainRepo(service)

    @Test
    fun initialRepoTest() = runTest {

        //given
        //when
        coEvery { service.getCountries() } coAnswers { FAKE_COUNTRIES }
        //then
        assertEquals(FAKE_COUNTRIES, repo.getCountries())
    }
}