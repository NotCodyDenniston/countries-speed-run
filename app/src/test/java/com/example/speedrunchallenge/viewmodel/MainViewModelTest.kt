package com.example.speedrunchallenge.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.speedrunchallenge.model.MainRepo
import com.example.speedrunchallenge.model.local.entities.Country
import com.example.speedrunchallenge.model.local.entities.Currency
import com.example.speedrunchallenge.model.local.entities.Language
import com.example.speedrunchallenge.utils.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.job
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class MainViewModelTest {

//    @RegisterExtension
//    val testExtension = CoroutinesTestExtension()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    val repo: MainRepo = mockk(relaxed = true)
    private lateinit var mainViewModel: MainViewModel

    @Test
    fun initialViewModelTest() = runTest {
        mainViewModel = MainViewModel(repo)
        lateinit var results: List<Country>
        //given
        //when
        coEvery { repo.getCountries() } coAnswers { FAKE_COUNTRIES }
        mainViewModel.getCountries().invokeOnCompletion {
         results = mainViewModel.state.value
        }
        //then
        assertEquals(true, true)
        assertEquals(FAKE_COUNTRIES, results)
    }

}

val FAKE_COUNTRIES: List<Country> = listOf(
    Country(
        capital = "Des Moin es", code = "IA", currency = Currency(
            code = "Gu",
            name = "Guap",
            symbol = "🥶"
        ), demonym = "", flag = "", language = Language(
            code = "En",
            isoName = "eng",
            name = "English",
            nativeName = "Extra English",
        ), name = "Iowa", region = "Midwest"
    ), Country(
        capital = "Des Moin es2", code = "IA2", currency = Currency(
            code = "Gu",
            name = "Guap",
            symbol = "🥶"
        ), demonym = "", flag = "", language = Language(
            code = "En",
            isoName = "eng",
            name = "English",
            nativeName = "Extra English",
        ), name = "Iowa2", region = "Midwest2"
    )
)